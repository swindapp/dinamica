Rails.application.routes.draw do
  

  resources :contrato_niveis
  resources :materias
  resources :niveis
  resources :atividade_complementares
  resources :formacoes
  resources :coordenacao_pedagogicas
  resources :planejamento_pedagogicos
  resources :capitulos
  resources :livros
  resources :disciplinas
  resources :series
  resources :niveis
  resources :prefeituras
  resources :cidades
  resources :estados
  resources :ano_letivos
  devise_for :users

  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  devise_scope :user  do
    
    get  'users/new' => 'users#new'
    post 'users/create' => 'users#create'
    patch 'users/update' => 'users#update'
  end

  root 'home#index'

  get 'docs/docs_buildnotes'
  get 'intel/intel_introduction'
  get 'intel/intel_marketing_dashboard'
  get 'page/page_inbox_general'
  get 'page/page_search'
  get 'utilities/utilities_responsive_grid'

  resources :contratos do
    resources :contrato_niveis
  end

  

  resources :coordenadores do
    collection do
      get 'consulta/:serie_id/:livro_id', to: 'coordenadores#consulta', as: 'consulta'
    end
  end

end
