Rails.configuration.app = 'Dinâmica'
Rails.configuration.app_name = 'Dinâmica'
Rails.configuration.app_flavor = 'Dinâmica'
Rails.configuration.app_flavor_subscript = ''
Rails.configuration.user = ''
Rails.configuration.email = 
Rails.configuration.twitter = ''
Rails.configuration.avatar = 'avatar_dinamica.png'
Rails.configuration.version = '4.5.2'
Rails.configuration.bs4v = '4.5'
Rails.configuration.logo = 'logo_circular.png'
Rails.configuration.logo_m = 'logo.png'
Rails.configuration.copyright = '2022 © iWind by&nbsp;<a href=\'http://www.swind.com.br/\' class=\'text-primary fw-500\' title=\'SWS\' target=\'_blank\'>SWS</a>'.html_safe
Rails.configuration.copyright_inverse = '2022 © iWind by&nbsp;<a href=\'http://www.swind.com.br/\' class=\'text-white opacity-40 fw-500\' title=\'SWS\' target=\'_blank\'>SWS</a>'.html_safe
Rails.configuration.icon_prefix = 'fal'
Rails.configuration.layout_settings = true
Rails.configuration.chat_interface = false
Rails.configuration.shortcut_menu = false
Rails.configuration.app_layout_shortcut = true
Rails.configuration.app_shortcut_modal = false
Rails.configuration.app_header = true
Rails.configuration.app_footer = true
Rails.configuration.app_sidebar = true
Rails.configuration.production_ready = false
Rails.configuration.google_analytics_id = ''
Rails.configuration.notification_menu_bar = false
Rails.configuration.dropdown_app = false
Rails.configuration.search_menu_bar = false
Rails.configuration.nav_filter = false
Rails.configuration.nav_info_card = false


