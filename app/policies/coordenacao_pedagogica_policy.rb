class CoordenacaoPedagogicaPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    ['admin','gestao_dinamica'].include?(user.perfil)
  end

  def show?
    ['admin','gestao_dinamica'].include?(user.perfil)
  end

  def new?
    ['admin','gestao_dinamica'].include?(user.perfil)
  end

  def edit?
    ['admin','gestao_dinamica'].include?(user.perfil)
  end
end
