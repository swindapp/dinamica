json.extract! contrato_nivel, :id, :contrato_id, :nivel_id, :ativo, :created_at, :updated_at
json.url contrato_nivel_url(contrato_nivel, format: :json)
