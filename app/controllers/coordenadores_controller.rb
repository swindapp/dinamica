class CoordenadoresController < ApplicationController
  before_action :set_livro, only: [:consulta]
  before_action :set_livros, only: [:consulta]
  before_action :set_serie, only: [:consulta]
  before_action :set_midias, only: [:consulta]
  before_action :set_niveis, only: [:consulta]

  def consulta
  end
 

  private

  def set_livro
    @livro = Livro.find(params[:livro_id]) if params[:livro_id].present? and params[:livro_id].to_i > 0
  end

  def set_livros
    @livros = Livro.where(serie_id: params[:serie_id]) if params[:serie_id].present? and params[:serie_id].to_i > 0
  end

  def set_serie
    @serie = Serie.find(params[:serie_id]) if params[:serie_id].present? and params[:serie_id].to_i > 0
  end

  def set_niveis
    @niveis = Nivel.where(id: CoordenacaoPedagogica.where(user_id: current_user).where(ano_letivo_id: 1).pluck(:nivel_id).uniq).order(:nome)
  end

  def set_midias
    if params[:serie_id].present? and params[:serie_id].to_i > 0 and @livro.present?
      @atividade_complementares = AtividadeComplementar.where(capitulo_id: @livro.capitulos.pluck(:id))
    end
  end

end
