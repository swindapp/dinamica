class PrefeiturasController < ApplicationController
  before_action :set_prefeitura, only: [:show, :edit, :update, :destroy]

  # GET /prefeituras
  def index
    # @search = Prefeitura.search(params[:q])
    # @prefeituras = @search.result
    authorize Serie
    @prefeituras = Prefeitura.all
  end

  # GET /prefeituras/1
  def show
    authorize Serie
  end

  # GET /prefeituras/new
  def new
    authorize Serie
    @prefeitura = Prefeitura.new
  end

  # GET /prefeituras/1/edit
  def edit
    authorize Serie
  end

  # POST /prefeituras
  def create
    @prefeitura = Prefeitura.new(prefeitura_params)

    if @prefeitura.save
      redirect_to @prefeitura, notice: 'Registro criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /prefeituras/1
  def update
    if @prefeitura.update(prefeitura_params)
      redirect_to @prefeitura, notice: 'Registro atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /prefeituras/1
  def destroy
    if @prefeitura.contratos.exists? 
      redirect_to prefeituras_url,  alert:"Existe contrato associado a prefeitura #{@prefeitura.nome}. Exclusão não permitida."
    elsif @prefeitura.coordenacao_pedagogicas.exists? 
      redirect_to prefeituras_url,  alert:"Existe coordenação pedagogica associda a prefeitura #{@prefeitura.nome}. Exclusão não permitida."
    else
      @prefeitura.destroy
      redirect_to prefeituras_url, notice: 'Registro removido com sucesso.'
    end
   
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_prefeitura
      @prefeitura = Prefeitura.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def prefeitura_params
      params.require(:prefeitura).permit(:nome, :cidade_id)
    end
end
