class SeriesController < ApplicationController
  before_action :set_serie, only: [:show, :edit, :update, :destroy]
  #before_action :authorize_capitulo
  # GET /series
  def index
    # @search = Serie.search(params[:q])
    # @series = @search.result
    authorize Serie
    @series = Serie.all
  end

  # GET /series/1
  def show
    authorize Serie
  end

  # GET /series/new
  def new
    authorize Serie
    @serie = Serie.new
  end

  # GET /series/1/edit
  def edit
    authorize Serie
  end

  # POST /series
  def create
    @serie = Serie.new(series_params)

    if @serie.save
      redirect_to @serie, notice: 'Registro criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /series/1
  def update
    if @serie.update(series_params)
      redirect_to @serie, notice: 'Registro atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /series/1
  def destroy
   
    if @serie.livros.exists? 
      redirect_to @serie,  alert:"Existe livro associado a serie #{@serie.nome}. Exclusão não permitida."
    else
      @serie.destroy
    redirect_to @serie, notice: 'Registro removido com sucesso.'
    end
  end
  def authorize_capitulo
    authorize Capitulo
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_serie
      @serie = Serie.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def series_params
      params.require(:serie).permit(:nome, :codigo, :nivel_id)
    end
end
