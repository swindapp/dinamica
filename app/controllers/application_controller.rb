class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	before_action :authenticate_user!
    layout :layout_by_resource
	include Pundit::Authorization
	rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
 
private
 
def user_not_authorized
  redirect_to root_path
end
	protected
	def layout_by_resource
		if devise_controller? or self.action_name == "manutencao"
			"auth"
		else
			"application"
		end
	end

end
