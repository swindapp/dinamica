class DisciplinasController < ApplicationController
  before_action :set_disciplina, only: [:show, :edit, :update, :destroy]

  # GET /disciplinas
  def index
    # @search = Disciplina.search(params[:q])
    # @disciplinas = @search.result
    authorize Serie
    @disciplinas = Disciplina.all
  end

  # GET /disciplinas/1
  def show
    authorize Serie
  end

  # GET /disciplinas/new
  def new
    authorize Serie
    @disciplina = Disciplina.new

  end

  # GET /disciplinas/1/edit
  def edit
    authorize Serie
  end

  # POST /disciplinas
  def create
    @disciplina = Disciplina.new(disciplina_params)

    if @disciplina.save
      redirect_to @disciplina, notice: 'Registro criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /disciplinas/1
  def update
    if @disciplina.update(disciplina_params)
      redirect_to @disciplina, notice: 'Registro atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /disciplinas/1
  def destroy

    if @disciplina.capitulos.exists? 
      redirect_to @disciplina,  alert:"Existe capitulo associado a disciplina #{@disciplina.nome}. Exclusão não permitida."
    else
      @disciplina.destroy
      redirect_to disciplinas_url, notice: 'Registro removido com sucesso.'
    end
      
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_disciplina
      @disciplina = Disciplina.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def disciplina_params
      params.require(:disciplina).permit(:nome, :codigo, :materia_id)
    end
end
