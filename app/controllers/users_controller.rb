class UsersController < ApplicationController
	before_action :set_user, only: [:show, :edit, :update, :destroy, :save]

	# GET /users
	def index
		# @search = user.search(params[:q])
		# @users = @search.result
		authorize Serie
		@users = User.order(:email)
	end

	# GET /users/1
	def show
		authorize Serie
	end



	


	# GET /users/new
	def new
		authorize Serie
		@user = User.new
	end

	# GET /users/1/edit
	def edit
		authorize Serie
	end

	# POST /users
	def create
		@user = User.new(users_params.merge(password: '12345678', password_confirmation: '12345678'))
		if @user.save()
			redirect_to @user, notice: 'Registro atualizado com sucesso.'
		else
			render :new
		end
	end

	

	# PATCH/PUT /users/1
	def update
		@user = User.find(params[:id])
			if params[:user][:password_antiga].present? 
				if @user.valid_password?(params[:user][:password_antiga])
						if @user.update(users_params)
							redirect_to User, notice: 'Registro atualizado com sucesso.'
						else
							render :edit
						end
					else
						render :edit
					end
				else
					render :edit
				end
			
	end

	# DELETE /users/1
	def destroy
		@user.destroy
		redirect_to users_url, notice: 'Registro removido com sucesso.'
	end



	
	private
		# Use callbacks to share common setup or constraints between actions.
		def set_user
			@user = User.find(params[:id])
		end

		# Only allow a trusted parameter "white list" through.
		def users_params
			params.require(:user).permit(:perfil,:email,:cidade_id,:password,:password_confirmation)
		end
		
end
  