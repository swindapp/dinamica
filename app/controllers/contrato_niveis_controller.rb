class ContratoNiveisController < ApplicationController
  before_action :set_contrato, only: [:new]
  before_action :set_contrato_nivel, only: [:show, :edit, :update, :destroy]

  # GET /contrato_niveis
  def index
    # @search = ContratoNivel.search(params[:q])
    # @contrato_niveis = @search.result
    @contrato_niveis = ContratoNivel.all
  end

  # GET /contrato_niveis/1
  def show
  end

  # GET /contrato_niveis/new
  def new
    @contrato_nivel = ContratoNivel.new(contrato_id: @contrato.id, ativo: true)
  end

  # GET /contrato_niveis/1/edit
  def edit
  end

  # POST /contrato_niveis
  def create
    @contrato_nivel = ContratoNivel.new(contrato_nivel_params)

    if @contrato_nivel.save
      redirect_to @contrato_nivel, notice: 'Registro criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /contrato_niveis/1
  def update
    if @contrato_nivel.update(contrato_nivel_params)
      redirect_to contratos_url, notice: 'Registro atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /contrato_niveis/1
  def destroy
    @contrato_nivel.destroy
    redirect_to contratos_url, notice: 'Registro removido com sucesso.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contrato_nivel
      @contrato_nivel = ContratoNivel.find(params[:id])
    end
    def set_contrato
      @contrato = Contrato.find(params[:contrato_id])
    end

    # Only allow a trusted parameter "white list" through.
    def contrato_nivel_params
      params.require(:contrato_nivel).permit(:contrato_id, :nivel_id, :ativo)
    end
end
