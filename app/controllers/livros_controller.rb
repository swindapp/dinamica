class LivrosController < ApplicationController
  before_action :set_livro, only: [:show, :edit, :update, :destroy]

  # GET /livros
  def index
    # @search = Livro.search(params[:q])
    # @livros = @search.result
    authorize Serie
    @livros = Livro.all
  end

  # GET /livros/1
  def show
    authorize Serie
  end

  # GET /livros/new
  def new
    authorize Serie
    @livro = Livro.new
  end

  # GET /livros/1/edit
  def edit
    authorize Serie
  end

  # POST /livros
  def create
    @livro = Livro.new(livro_params)

    if @livro.save
      redirect_to @livro, notice: 'Registro criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /livros/1
  def update
    if @livro.update(livro_params)
      redirect_to @livro, notice: 'Registro atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /livros/1
  def destroy
    if @livro.capitulos.exists? 
      redirect_to livros_url,  alert:"Existe capitulo associado ao livro #{@livro.nome}. Exclusão não permitida."
    else
      @livro.destroy
      redirect_to livros_url, notice: 'Registro removido com sucesso.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_livro
      @livro = Livro.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def livro_params
      params.require(:livro).permit(:nome, :serie_id, :volume, :ano_edicao)
    end
end
