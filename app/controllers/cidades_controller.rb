class CidadesController < ApplicationController
  before_action :set_cidade, only: [:show, :edit, :update, :destroy]


  # GET /cidades
  def index
    authorize Serie
    if params[:nome_cidade].present?    
      @cidades = Cidade.where("nome like ?", "%#{params[:nome_cidade].upcase}%").order(:nome).limit(1000)
    else
      @cidades = Cidade.order(:nome).limit(1000)
    end
  end

  def new
    authorize Serie
    @cidade = Cidade.new
  end

  # GET /cidades/1/edit
  def edit
    authorize Serie
  end

  # POST /cidades
  def create
    @cidade = Cidade.new(cidade_params)

    if @cidade.save
      redirect_to @cidade, notice: 'Registro criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /cidades/1
  def update
    if @cidade.update(cidade_params)
      redirect_to @cidade, notice: 'Registro atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /cidades/1
  def destroy
      binding.pry
      if @cidade.prefeituras.exists? 
        redirect_to cidades_url,  alert:"Existe coordenação pedagogica associda a cidade #{@cidade.nome}. Exclusão não permitida."
      
        
      else
        @cidade.destroy
        redirect_to cidades_url, notice: 'Registro removido com sucesso.'
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cidade
      @cidade = Cidade.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cidade_params
      params.require(:cidade).permit(:nome, :estado_id, :codigo_ibge)
    end
end
