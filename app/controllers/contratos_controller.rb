class ContratosController < ApplicationController
  before_action :set_contrato, only: [:show, :edit, :update, :destroy]

  # GET /contratos
  def index
    # @search = Contrato.search(params[:q])
    # @contratos = @search.result
    authorize Serie
    @contratos = Contrato.all
  end

  # GET /contratos/1
  def show
    authorize Serie
  end

  # GET /contratos/new

  def new
    authorize Serie
    @contrato = Contrato.new
  end

  # GET /contratos/1/edit
  def edit
    authorize Serie
  end

  # POST /contratos
  def create
    @contrato = Contrato.new(contrato_params)
    #@contrato.user_id = current_user.id

    if @contrato.save
      redirect_to @contrato, notice: 'Registro criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /contratos/1
  def update
    if @contrato.update(contrato_params)
      redirect_to @contrato, notice: 'Registro atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /contratos/1
  def destroy
    
    if @contrato.coordenacao_pedagogicas.exists? 
      redirect_to @contrato,  alert:"Existe coordenação pedagogica associada a contrato . Exclusão não permitida."
    else
      @contrato.destroy
    redirect_to contratos_url, notice: 'Registro removido com sucesso.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contrato
      @contrato = Contrato.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contrato_params
      params.require(:contrato).permit(:prefeitura_id, :ano_letivo_id)
    end
end
