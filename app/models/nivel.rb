class Nivel < ApplicationRecord
    has_many :coordenacao_pedagogicas
    has_many :series
    has_many :contrato_niveis
    has_many :contratos, through: :contrato_niveis

end
