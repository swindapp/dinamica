class Contrato < ApplicationRecord
  belongs_to :ano_letivo
  belongs_to :prefeitura
  belongs_to :ano_letivo
  has_many :coordenacao_pedagogicas
  has_many :contrato_niveis
  has_many :niveis, through: :contrato_niveis
  
end
