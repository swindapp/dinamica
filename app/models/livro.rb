class Livro < ApplicationRecord
  belongs_to :serie

  has_many :capitulos
end
