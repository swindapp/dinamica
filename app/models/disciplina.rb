class Disciplina < ApplicationRecord
  belongs_to :materia
  has_many :capitulos
end
