class AtividadeComplementar < ApplicationRecord
  belongs_to :capitulo
  enum tipo: {simulado_inep: 0, formacao_professor: 1, atividade_complementar: 2, atualizacao_material: 3}

  enum tipo_conteudo: {
    pdf: 1,
    video: 2
  }


  def icone_tipo_conteudo
    case self.tipo_conteudo
    when 'pdf'
      _icone = '<fal fa-file-pdf fa-2x'
      _color = 'primary'
    when 'video'
      _icone = 'fal fa-video fa-2x'
      _color = 'success'
    else
      _icone = 'fal fa-file fa-2x'
      _color = 'danger'
    end

    return {icone: _icone, cor_icone: _color}
  end


end
