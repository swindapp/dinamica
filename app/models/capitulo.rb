class Capitulo < ApplicationRecord
  belongs_to :livro
  belongs_to :disciplina

  has_many :atividade_complementares
end
