class Prefeitura < ApplicationRecord
  has_many :contratos
  has_many :coordenacao_pedagogicas
  
  belongs_to :cidade
end
