class ContratoNivel < ApplicationRecord
  belongs_to :contrato
  belongs_to :nivel

  validate :maxima_quantidade,:procura_duplicado, on: :create

  def procura_duplicado

    if ContratoNivel.where(contrato_id: self.contrato_id, nivel_id: self.nivel_id).count > 0
      errors.add(:contrato_id, "Já existe um contrato com esse nível!")
    end
    
  end

  def maxima_quantidade
    if ContratoNivel.where(contrato_id: self.contrato_id).count > 3
      errors.add(:contrato_id, "Quantidade máxima atingida!")
    end
  end

end
