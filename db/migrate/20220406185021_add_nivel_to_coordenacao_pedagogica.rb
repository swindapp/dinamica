class AddNivelToCoordenacaoPedagogica < ActiveRecord::Migration[6.0]
  def change
    add_reference :coordenacao_pedagogicas, :nivel, null: false, foreign_key: true
  end
end
