class AddReferencesCidadeToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :cidade, index: { name: 'idx_usr_01' }
  end
end
