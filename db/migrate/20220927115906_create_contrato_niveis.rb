class CreateContratoNiveis < ActiveRecord::Migration[6.0]
  def change
    create_table :contrato_niveis do |t|
      t.references :contrato, null: false, foreign_key: true, index: {name: 'idx_cn_01'}
      t.references :nivel, null: false, foreign_key: true, index: {name: 'idx_cn_02'}
      t.boolean :ativo

      t.timestamps
    end
  end
end
