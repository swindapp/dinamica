require 'test_helper'

class ContratoNiveisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contrato_nivel = contrato_niveis(:one)
  end

  test "should get index" do
    get contrato_niveis_url
    assert_response :success
  end

  test "should get new" do
    get new_contrato_nivel_url
    assert_response :success
  end

  test "should create contrato_nivel" do
    assert_difference('ContratoNivel.count') do
      post contrato_niveis_url, params: { contrato_nivel: { ativo: @contrato_nivel.ativo, contrato_id: @contrato_nivel.contrato_id, nivel_id: @contrato_nivel.nivel_id } }
    end

    assert_redirected_to contrato_nivel_url(ContratoNivel.last)
  end

  test "should show contrato_nivel" do
    get contrato_nivel_url(@contrato_nivel)
    assert_response :success
  end

  test "should get edit" do
    get edit_contrato_nivel_url(@contrato_nivel)
    assert_response :success
  end

  test "should update contrato_nivel" do
    patch contrato_nivel_url(@contrato_nivel), params: { contrato_nivel: { ativo: @contrato_nivel.ativo, contrato_id: @contrato_nivel.contrato_id, nivel_id: @contrato_nivel.nivel_id } }
    assert_redirected_to contrato_nivel_url(@contrato_nivel)
  end

  test "should destroy contrato_nivel" do
    assert_difference('ContratoNivel.count', -1) do
      delete contrato_nivel_url(@contrato_nivel)
    end

    assert_redirected_to contrato_niveis_url
  end
end
