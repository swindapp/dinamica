require "application_system_test_case"

class ContratoNiveisTest < ApplicationSystemTestCase
  setup do
    @contrato_nivel = contrato_niveis(:one)
  end

  test "visiting the index" do
    visit contrato_niveis_url
    assert_selector "h1", text: "Contrato Niveis"
  end

  test "creating a Contrato nivel" do
    visit contrato_niveis_url
    click_on "New Contrato Nivel"

    check "Ativo" if @contrato_nivel.ativo
    fill_in "Contrato", with: @contrato_nivel.contrato_id
    fill_in "Nivel", with: @contrato_nivel.nivel_id
    click_on "Create Contrato nivel"

    assert_text "Contrato nivel was successfully created"
    click_on "Back"
  end

  test "updating a Contrato nivel" do
    visit contrato_niveis_url
    click_on "Edit", match: :first

    check "Ativo" if @contrato_nivel.ativo
    fill_in "Contrato", with: @contrato_nivel.contrato_id
    fill_in "Nivel", with: @contrato_nivel.nivel_id
    click_on "Update Contrato nivel"

    assert_text "Contrato nivel was successfully updated"
    click_on "Back"
  end

  test "destroying a Contrato nivel" do
    visit contrato_niveis_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Contrato nivel was successfully destroyed"
  end
end
